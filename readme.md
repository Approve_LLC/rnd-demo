## Development

1) Скопировать .env.example => .env  
    Проверить\настроить в env параметры:
    - MINIO_ENDPOINT
    - RAISA_DOMAIN
    - CHEF_DOMAIN     
    - REDIS_HOST     
    - REDIS_PORT     
    - DB_HOST     
    - DB_DATABASE     
    - DB_USERNAME     
    - DB_PASSWORD
    
2) Выполнить  
   
```

docker-compose -f docker-compose.yml up

docker-compose run app php artisan migrate

```
