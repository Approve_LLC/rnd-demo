<?php

namespace App;

use App\Interfaces\SyncInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\MenuDishComponent
 *
 * @property string $id
 * @property string $menu_dish_composition_id
 * @property string $packed_product_id
 * @property float $quantity
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property MenuDishComposition $menuDishComposition
 * @property PackedProduct $packedProduct
 * @property MeasureUnit $measureUnit
 * @property \App\MenuDishComposition $composition
 */
class MenuDishComponent extends Model
{
    use SoftDeletes;

    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['menu_dish_composition_id', 'packed_product_id', 'quantity', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function composition()
    {
        return $this->belongsTo('App\MenuDishComposition', 'menu_dish_composition_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function packedProduct()
    {
        return $this->belongsTo('App\PackedProduct');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function measureUnit()
    {
        return $this->belongsTo('App\MeasureUnit');
    }
}
