<?php

namespace App;

use App\Interfaces\SyncInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * App\PackedProductItem
 *
 * @property string $id
 * @property string $packed_product_id
 * @property string $nomenclature_id
 * @property string $measure_unit_id
 * @property int $ext_id
 * @property float $quantity
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property PackedProduct $packedProduct
 * @property Nomenclature $nomenclature
 * @property MeasureUnit $measureUnit
 */
class PackedProductItem extends Model implements SyncInterface
{
    use SoftDeletes;

    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['packed_product_id', 'nomenclature_id', 'ext_id', 'quantity', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function packedProduct()
    {
        return $this->belongsTo('App\PackedProduct');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function nomenclature()
    {
        return $this->belongsTo('App\Nomenclature');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function measureUnit()
    {
        return $this->belongsTo('App\MeasureUnit');
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function setSyncContent($data)
    {
        $this->ext_id = $data->id;

        $packedProduct = PackedProduct::where(['ext_id' => $data->packing_id])->first();
        if (empty($packedProduct)) {
            throw new NotFoundHttpException("Сперва необходимо загрузить PackedProduct $data->packing_id!");
        }
        $this->packedProduct()->associate($packedProduct);

        $nomenclature = Nomenclature::where(['ext_id' => $data->nomenclature_id])->first();
        if (empty($nomenclature)) {
            throw new NotFoundHttpException("Сперва необходимо загрузить Nomenclature $data->nomenclature_id!");
        }
        $this->nomenclature()->associate($nomenclature);

        $measureUnit = MeasureUnit::where(['ext_id' => $data->measure_unit_id])->first();
        if (empty($measureUnit)) {
            throw new NotFoundHttpException("Сперва необходимо загрузить MeasureUnit $data->measure_unit_id!");
        }
        $this->measureUnit()->associate($measureUnit);
        $this->quantity = $data->quant;
        $this->save();
    }
}
