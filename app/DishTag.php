<?php

namespace App;

use App\Interfaces\DownloadInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $ext_id
 * @property string $name
 * @property boolean $is_shown
 * @property boolean $is_used_for_filtering
 * @property boolean $is_used_for_sorting
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class DishTag extends Model implements DownloadInterface
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['ext_id', 'name', 'is_shown', 'is_used_for_filtering', 'is_used_for_sorting', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function dishes()
    {
        return $this->belongsToMany('App\Dish');
    }

    public function getStorageKey()
    {
        return 'dish-tag';
    }

    public function downloadContent($data, DownloadContext $context)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        $this->is_shown = $data->is_shown;
        $this->is_used_for_filtering = $data->is_used_for_filtering;
        $this->is_used_for_sorting = $data->is_used_for_sorting;
        $this->save();
    }
}
