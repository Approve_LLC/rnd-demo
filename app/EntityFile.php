<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\EntityFile
 *
 * @property string $id
 * @property string $entity_id
 * @property string $entity_type
 * @property string|null $entity_file_version_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \App\EntityFileVersion|null $current
 * @property \Illuminate\Database\Eloquent\Model $entity
 * @property \Illuminate\Database\Eloquent\Collection|\App\EntityFileVersion[] $versions
 */
class EntityFile extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    public $incrementing = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function entity()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function current()
    {
        return $this->belongsTo('App\EntityFileVersion', 'entity_file_version_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function versions()
    {
        return $this->hasMany('App\EntityFileVersion', 'object_storage_bucket_id');
    }

}
