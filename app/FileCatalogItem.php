<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $parent_id
 * @property string $name
 * @property boolean $is_folder
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property FileCatalogItem $parent
 */
class FileCatalogItem extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['parent_id', 'name', 'is_folder', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\FileCatalogItem', 'parent_id');
    }

    public function file()
    {
        return $this->morphOne('App\EntityFile', 'entity');
    }
}
