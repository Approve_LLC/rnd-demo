<?php

namespace App;


use App\Interfaces\FileManagerInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Interfaces\DownloadInterface;
use stdClass;

/**
 * App\Inventory
 *
 * @property string $id
 * @property int $ext_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \App\EntityFile $picture
 */
class Inventory extends Model implements DownloadInterface, FileManagerInterface
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    public $incrementing = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function picture()
    {
        return $this->morphOne('App\EntityFile', 'entity');
    }

    /**
     * @param mixed $data
     * @param DownloadContext $context
     */
    public function downloadContent($data, DownloadContext $context)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        $this->save();
        if (!$this->picture()->exists()) {
            //какой урл у них?
            $imgUrl = config('app.raisa_domain') . '/' . $data->image;
            $entityFile = $context
                ->getFileManagerService()
                ->downloadEntityFileVersion($this, $data, $imgUrl, $context->bucket);
            $key = $entityFile->current->key;
            $context->getFileManagerService()->uploadFileByUrl($imgUrl, $key);
            $this->picture()->save($entityFile);
            $this->save();
        }

    }

    /**
     * @return string
     */
    public function getStorageKey()
    {
        return 'inventory';
    }
}
