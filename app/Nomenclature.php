<?php

namespace App;

use App\Interfaces\DownloadInterface;
use App\Interfaces\FileManagerInterface;
use App\Interfaces\SyncInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * App\Nomenclature
 *
 * @property string $id
 * @property int ext_id
 * @property string $name
 * @property string $group_id
 * @property bool $is_packing
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \App\NomenclatureGroup $group
 * @property \App\EntityFile $picture
 */
class Nomenclature extends Model implements SyncInterface, DownloadInterface, FileManagerInterface
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';
    
    public $incrementing = true;

    public function group()
    {
        return $this->belongsTo('App\NomenclatureGroup', 'group_id');
    }

    public function picture()
    {
        return $this->morphOne('App\EntityFile', 'entity');
    }

    public function downloadContent($data, DownloadContext $context)
    {
        $fileUrl = config('app.raisa_domain') . '/' . $data->image_url;
        $bucket = $context->bucket;
        if (!$this->picture()->exists()) {
            $entityFile = $context
                ->getFileManagerService()
                ->downloadEntityFileVersion($this, $data, $fileUrl, $bucket);
            $key = $entityFile->current->key;
            $context->getFileManagerService()->uploadFileByUrl($data->image_url, $key);
            $this->picture()->save($entityFile);
            $this->save();
        }
    }

    public function setSyncContent($data)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        $nomenclatureGroup = NomenclatureGroup::where(['ext_id' => $data->nomenclature_group_id])->first();
        if (empty($nomenclatureGroup)) {
            throw new NotFoundHttpException("Сперва необходимо загрузить NomenclatureGroup $data->nomenclature_group_id!");
        }
        if (!empty($nomenclatureGroup)) {
            $this->group()->associate($nomenclatureGroup);
            //проблема в том, что раиса не отдает номенклатуры с такими групп_ид
            //решение: сделать так, чтобы раиса отдавала с такими групп_ид
            if ($nomenclatureGroup->ext_id == NomenclatureGroup::NOMENCLATURE_GROUP_PACKING_ID_FIRST
                || $nomenclatureGroup->ext_id == NomenclatureGroup::NOMENCLATURE_GROUP_PACKING_ID_SECOND
            ) {
                $this->is_packing = true;
            } else {
                $this->is_packing = false;
            }
        } else {
            $this->is_packing = false;
        }
        $this->save();
    }

    public function getStorageKey()
    {
        return 'nomenclature';
    }

}
