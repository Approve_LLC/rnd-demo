<?php

namespace App;

use App\Interfaces\DownloadInterface;
use App\Interfaces\FileManagerInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Chef
 *
 * @property string $id
 * @property string $ext_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \App\EntityFile $picture
 */
class Chef extends Model implements DownloadInterface, FileManagerInterface
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    public $incrementing = true;

    public function picture()
    {
        return $this->morphOne('App\EntityFile', 'entity');
    }

    /**
     * @param mixed $data
     * @param DownloadContext $context
     */
    public function downloadContent($data, DownloadContext $context)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        $this->save();
        //надо ли проверять, что файл существует еще и физически или достаточно наличия в базе?
        if (!$this->picture()->exists()) {
            $bucket = $context->bucket;
            $entityFile = $context
                ->getFileManagerService()
                ->downloadEntityFileVersion($this, $data, $data->image_url, $bucket);
            $key = $entityFile->current->key;
            $context->getFileManagerService()->uploadFileByUrl($data->image_url, $key);
            $this->picture()->save($entityFile);
            $this->save();
        }
    }

    /**
     * @return string
     */
    public function getStorageKey()
    {
        return 'chef';
    }

}
