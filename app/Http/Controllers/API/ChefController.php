<?php

namespace App\Http\Controllers\API;

use App\Chef;
use App\DownloadContext;
use App\Http\Controllers\Controller;
use App\Http\Resources\ChefResource;
use App\Inventory;
use App\Services\FileManagerService\FileManagerService;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ChefController
 * @package App\Http\Controllers\API
 */
class ChefController extends Controller
{

    /**
     * @var FileManagerService
     */
    private $fileManagerService;

    public function __construct(FileManagerService $fileManagerService)
    {
        $this->fileManagerService = $fileManagerService;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $chefs = Chef::paginate();
        $collection = ChefResource::collection($chefs);
        return $collection;
    }

    /**
     * @param $id
     * @return ChefResource
     */
    public function show($id)
    {
        $chef = Chef::where(['id' => $id])->first();
        $resource = new ChefResource($chef);
        return $resource;
    }

    /**
     * @param Request $request
     * @return ChefResource
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string'
        ]);
        $chef = new Chef([
            'ext_id' => $request->extId,
            'name' => $request->name
        ]);
        $chef->save();
        $resource = new ChefResource($chef);
        return $resource;
    }

    /**
     * @param Request $request
     * @param $id
     * @return ChefResource
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string'
        ]);
        $chef = Chef::where(['id' => $id])->first();
        $chef->name = $request->name;
        $chef->save();
        $resource = new ChefResource($chef);
        return $resource;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        Chef::where(['id' => $id])->delete();
        return response()->json([
            'data' => 'Deleted!'
        ], 200);
    }

    /**
     * @param DefaultUploadImageRequest $request
     * @param $id
     */
    public function uploadImage(DefaultUploadImageRequest $request, $id)
    {
        if ($request->file('file')->isValid()) {
            $file = $request->file('file');
            $fileName = $file->getFilename();
            $context = new DownloadContext($this->fileManagerService);
            $data = [
                'name' => $request->name,
                'description' => $request->description,
            ];
            $bucket = $context->bucket;
            $model = Inventory::query()->where(['id' => $id])->first();
            if (empty($inventory)) {
                throw new NotFoundHttpException("Инструмент номер $id не найден.");
            }
            if ($model->has('picture')->get()) {
                $entityFile = $context->getFileManagerService()->updateEntity($model->id, $data, $fileName, $bucket);
            } else {
                $entityFile = $context->getFileManagerService()->downloadEntityFileVersion($model, $data, $fileName, $bucket);
            }
            $key = $entityFile->current->key;
            $context->getFileManagerService()->uploadFile($file, $key);
        }
    }

}
