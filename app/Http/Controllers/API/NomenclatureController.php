<?php

namespace App\Http\Controllers\API;

use App\DownloadContext;
use App\Http\Requests\DefaultUploadImageRequest;
use App\Http\Resources\NomenclatureResource;
use App\Nomenclature;
use App\Services\FileManagerService\FileManagerService;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class NomenclatureController
 * @package App\Http\Controllers\API
 */
class NomenclatureController extends Controller
{

    /**
     * @var FileManagerService
     */
    private $fileManagerService;

    public function __construct(FileManagerService $fileManagerService)
    {
        $this->fileManagerService = $fileManagerService;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $nomenclatures = Nomenclature::paginate();
        $collection = NomenclatureResource::collection($nomenclatures);
        return $collection;
    }

    /**
     * @param DefaultUploadImageRequest $request
     * @param $id
     * @throws NotFoundHttpException
     */
    public function uploadImage(DefaultUploadImageRequest $request, $id)
    {
        if ($request->file('file')->isValid()) {
            $file = $request->file('file');
            $fileName = $file->getFilename();
            $context = new DownloadContext($this->fileManagerService);
            $data = [
                'name' => $request->name,
                'description' => $request->description,
            ];
            $bucket = $context->bucket;
            $model = Nomenclature::query()->where(['id' => $id])->first();
            if (empty($inventory)) {
                throw new NotFoundHttpException("Инструмент номер $id не найден.");
            }
            if ($model->has('picture')->get()) {
                $entityFile = $context->getFileManagerService()->updateEntity($model->id, $data, $fileName, $bucket);
            } else {
                $entityFile = $context->getFileManagerService()->downloadEntityFileVersion($model, $data, $fileName, $bucket);
            }
            $key = $entityFile->current->key;
            $context->getFileManagerService()->uploadFile($file, $key);
        }
    }
}
