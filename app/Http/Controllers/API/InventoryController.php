<?php

namespace App\Http\Controllers\API;

use App\DownloadContext;
use App\Http\Requests\DefaultUploadImageRequest;
use App\Http\Requests\InventoryRequest;
use App\Http\Resources\InventoryResource;
use App\Inventory;
use App\Services\FileManagerService\FileManagerService;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class InventoryController
 * @package App\Http\Controllers\API
 * @group Inventory
 */
class InventoryController extends Controller
{

    /**
     * @var FileManagerService
     */
    private $fileManagerService;

    public function __construct(FileManagerService $fileManagerService)
    {
        $this->fileManagerService = $fileManagerService;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $inventories = Inventory::query()->paginate();
        $collection = InventoryResource::collection($inventories);
        return $collection;
    }

    /**
     * @param $id
     * @return InventoryResource
     */
    public function show($id)
    {
        $inventory = Inventory::query()->where(['id' => $id])->first();
        if (empty($inventory)) {
            throw new NotFoundHttpException("Инструмент номер $id не найден.");
        }
        $resource = new InventoryResource($inventory);
        return $resource;
    }

    /**
     * @param InventoryRequest $request
     * @return InventoryResource
     */
    public function create(InventoryRequest $request)
    {
        $inventory = new Inventory([
            'name' => $request->name
        ]);
        $inventory->save();
        $resource = new InventoryResource($inventory);
        return $resource;
    }

    /**
     * @param InventoryRequest $request
     * @param $id
     * @return InventoryResource
     */
    public function update(InventoryRequest $request, $id)
    {
        $inventory = Inventory::query()->where(['id' => $id])->first();
        if (empty($inventory)) {
            throw new NotFoundHttpException("Инструмент номер $id не найден.");
        }
        $inventory->name = $request->name;
        $inventory->save();
        $resource = new InventoryResource($inventory);
        return $resource;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $inventory = Inventory::query()->where(['id' => $id]);
        if (empty($inventory)) {
            throw new NotFoundHttpException("Инструмент номер $id не найден.");
        }
        $inventory->delete();
        return response()->json([
            'data' => 'Deleted!'
        ], 200);
    }

    /**
     * @param DefaultUploadImageRequest $request
     * @param $id
     */
    public function uploadImage(DefaultUploadImageRequest $request, $id)
    {
        if ($request->file('file')->isValid()) {
            $file = $request->file('file');
            $fileName = $file->getFilename();
            $context = new DownloadContext($this->fileManagerService);
            $data = [
                'name' => $request->name,
                'description' => $request->description,
            ];
            $bucket = $context->bucket;
            $model = Inventory::query()->where(['id' => $id])->first();
            if (empty($inventory)) {
                throw new NotFoundHttpException("Инструмент номер $id не найден.");
            }
            if ($model->has('picture')->get()) {
                $entityFile = $context->getFileManagerService()->updateEntity($model->id, $data, $fileName, $bucket);
            } else {
                $entityFile = $context->getFileManagerService()->downloadEntityFileVersion($model, $data, $fileName, $bucket);
            }
            $key = $entityFile->current->key;
            $context->getFileManagerService()->uploadFile($file, $key);
        }
    }

}
