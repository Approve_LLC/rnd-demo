<?php

namespace App\Http\Controllers\API;

use App\DownloadContext;
use App\Http\Requests\DefaultUploadImageRequest;
use App\Http\Requests\PromotionSetChangeOrderRequest;
use App\Http\Requests\PromotionSetRequest;
use App\PromotionSet;
use App\Services\FileManagerService\FileManagerService;
use App\Http\Controllers\Controller;
use App\Http\Resources\PromotionSetResource;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PromotionSetController
 * @package App\Http\Controllers\API
 * @group PromotionSet
 */
class PromotionSetController extends Controller
{

    /**
     * @var FileManagerService
     */
    private $fileManagerService;

    public function __construct(FileManagerService $fileManagerService)
    {
        $this->fileManagerService = $fileManagerService;
    }

    /**
     * @param PromotionSetRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(PromotionSetRequest $request)
    {
        $promotionSets = PromotionSet::paginate();
        if ($request->has('name')) {
            $promotionSets = $promotionSets->where('name', 'like', '%' . $request->name . '%');
        }
        $promotionSets = $promotionSets->all();
        $collection = PromotionSetResource::collection($promotionSets);
        return $collection;
    }

    /**
     * @param $id
     * @return PromotionSetResource
     */
    public function show($id)
    {
        $promotionSet = PromotionSet::query()->where(['id' => $id])->first();
        if (empty($promotionSet)) {
            throw new NotFoundHttpException("Акционный сет $id не найден.");
        }
        $resource = new PromotionSetResource($promotionSet);
        return $resource;
    }

    /**
     * @param DefaultUploadImageRequest $request
     * @param $promotionSetId
     * @param $galleryItemId
     */
    public function uploadImage(DefaultUploadImageRequest $request, $promotionSetId, $galleryItemId)
    {
        if ($request->file('file')->isValid()) {
            $file = $request->file('file');
            $fileName = $file->getFilename();
            $context = new DownloadContext($this->fileManagerService);
            $data = [
                'name' => $request->name,
                'description' => $request->description,
            ];
            $bucket = $context->bucket;
            $promotionSet = PromotionSet::query()->where(['id' => $promotionSetId])->first();
            $galleryItem = $promotionSet->gallery()->firstOrNew(['id' => $galleryItemId]);
            $galleryItem->owner()->associate($promotionSet);
            $galleryItem->order_idx = $galleryItem->order_idx??$promotionSet->gallery()->max('order_idx')??0;
            $galleryItem->save();
            if ($galleryItem->has('picture')->get()) {
                $entityFile = $context->getFileManagerService()->updateEntity($galleryItem->id, $data, $fileName, $bucket);
            } else {
                $entityFile = $context->getFileManagerService()->downloadEntityFileVersion($galleryItem, $data, $fileName, $bucket);
            }
            $key = $entityFile->current->key;
            $context->getFileManagerService()->uploadFile($file, $key);
        }
    }

    /**
     * @param PromotionSetChangeOrderRequest $request
     * @param $promotionSetId
     * @param $galleryItemId
     */
    public function changeOrder(PromotionSetChangeOrderRequest $request, $promotionSetId, $galleryItemId)
    {
        $request->validated();
        $promotionSet = PromotionSet::query()->where(['id' => $promotionSetId])->first();
        if (empty($promotionSet)) {
            throw new NotFoundHttpException("Акционный сет $promotionSetId не найден.");
        }
        $item = $promotionSet->gallery()->where(['id' => $galleryItemId])->first();
        $item->order_idx = $request->order;
        $item->save();
    }

}
