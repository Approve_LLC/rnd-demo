<?php

namespace App\Http\Controllers\API;

use App\DownloadContext;
use App\Http\Requests\Dish\DishRequest;
use App\Http\Requests\Dish\UpdateDishRequest;
use App\Http\Requests\Dish\UploadStepImageRequest;
use App\Http\Resources\DishResource;
use App\Dish;
use App\RecipeStep;
use App\Services\FileManagerService\FileManagerService;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use PDF;

/**
 * Class DishController
 * @package App\Http\Controllers\API
 * @group Dish
 */
class DishController extends Controller
{

    /**
     * @var FileManagerService
     */
    private $fileManagerService;

    public function __construct(FileManagerService $fileManagerService)
    {
        $this->fileManagerService = $fileManagerService;
    }

    /**
     * api/dishes
     *
     * Получает список блюд
     * @bodyParam tags array Фильтр по тегам. Example: []
     * @bodyParam name string Фильтр по имени.
     * @param DishRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(DishRequest $request)
    {
        if ($request->has('tags')) {
            $dishes = Dish::with('tags')->whereIn('id', $request->tags);
        } else {
            $dishes = Dish::query();
        }
        if ($request->has('name')) {
            $dishes = $dishes->where('name', 'like', '%' . $request->name . '%');
        }
        $dishes = $dishes->paginate();
        $collection = DishResource::collection($dishes);
        return $collection;
    }

    /**
     * api/dishes/{id}
     *
     * Получает блюдо
     *
     * @param $id
     * @return DishResource
     */
    public function show($id)
    {
        $dish = Dish::query()->where(['id' => $id])->first();
        if (empty($dish)) {
            throw new NotFoundHttpException("Блюдо номер $id не найдено.");
        }
        $resource = new DishResource($dish);
        return $resource;
    }

    /**
     * api/dishes/{id}
     *
     * Обновляет информацию о блюде
     *
     * @bodyParam name string Фильтр по имени.
     * @bodyParam tip_text string Фильтр по подсказке повара.
     * @bodyParam shelf_life integer Срок годности.
     * @bodyParam net_weight integer Масса нетто.
     * @bodyParam cooking_time integer Время приготовления.
     * @bodyParam difficulty string Сложность приготовления.
     * @bodyParam protein integer Белки.
     * @bodyParam fat integer Жиры.
     * @bodyParam carb integer Carb.
     * @bodyParam calories integer Калории.
     * @bodyParam currentRecipe.steps array Фильтр по шагам рецепта блюда.
     * @bodyParam currentRecipe.steps.*.description string Описание.
     * @bodyParam currentRecipe.steps.*.duration integer Длительность шага.
     * @bodyParam currentRecipe.steps.*.step_number integer Номер шага.
     * @bodyParam currentRecipe.steps.*.image.file file Изображение шага.
     * @bodyParam currentRecipe.steps.*.image.id string ID изображения шага.
     * @bodyParam currentRecipe.steps.*.image.name string Имя изображения шага.
     * @bodyParam currentRecipe.steps.*.image.description Описание изображения Номер шага.
     * @bodyParam inventory array Фильтр по инструменту.
     * @bodyParam inventory.*.name string Имя.
     * @bodyParam tags array Фильтр по тегам.
     * @bodyParam tags.*.name string Имя тега.
     * @bodyParam tags.*.is_shown boolean .
     * @bodyParam tags.*.is_used_for_filtering boolean .
     * @bodyParam tags.*.is_used_for_sorting boolean .
     * @param UpdateDishRequest $request
     * @param $id
     * @return DishResource
     */
    public function update(UpdateDishRequest $request, $id)
    {
        $dish = Dish::query()->where(['id' => $id])->first();
        if (empty($dish)) {
            throw new NotFoundHttpException("Блюдо номер $id не найдено.");
        }
        $dish->fill($request->input());

        if ($request->file('currentRecipe.steps.image.file')->isValid()) {
            $file = $request->file('currentRecipe.steps.image.file');
            $fileName = $file->getFilename();
            $context = new DownloadContext($this->fileManagerService);
            $data = [
                'name' => $request->name,
                'description' => $request->description,
            ];
            $bucket = $context->bucket;
            $stepId = $request->input('currentRecipe.steps.image.id');
            $recipeStep = $dish->currentRecipe->steps()->where(['id' => $stepId])->first();
            if (empty($recipeStep)) {
                throw new NotFoundHttpException("Шаг рецепта номер $recipeStep не найден.");
            }
            if ($recipeStep->has('picture')->get()) {
                $entityFile = $context->getFileManagerService()->updateEntity($recipeStep->id, $data, $fileName, $bucket);
            } else {
                $entityFile = $context->getFileManagerService()->downloadEntityFileVersion($recipeStep, $data, $fileName, $bucket);
            }
            $key = $entityFile->current->key;
            $context->getFileManagerService()->uploadFile($file, $key);
        }

        //не знаю, как нормально обновить вложенные модели
        $steps = $request->input('currentRecipe.steps') ?? [];
        foreach ($steps as $step) {
            $recipeStep = RecipeStep::query()->where(['id' => $step['id']])->first();
            $recipeStep->fill($step);
            $recipeStep->save();
        }

        $inventory = $request->input('inventory') ?? [];
        foreach ($inventory as $item) {
            $inventoryItem = $dish->inventory()->where(['id' => $item['id']])->first();
            $inventoryItem->fill($item);
            $inventoryItem->save();
        }

        $tags = $request->input('tags') ?? [];
        foreach ($tags as $tag) {
            $dishTag = $dish->tags()->where(['id' => $tag['id']])->first();
            $dishTag->fill($tag);
            $dishTag->save();
        }
        $dish->save();

        $resource = new DishResource($dish);
        return $resource;
    }


    public function generatePdfTest()
    {
        $pdf = PDF::loadView('pdf/test', [
            'dish' => [
                'name' => 'Янис',
            ],
        ]);
        return $pdf->download('test.pdf');
    }
}
