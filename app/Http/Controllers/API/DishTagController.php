<?php

namespace App\Http\Controllers\API;

use App\DishTag;

use App\Http\Requests\DishTagListRequest;
use App\Http\Requests\DishTagRequest;
use App\Http\Resources\DishTagResource;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zend\Diactoros\Request;

/**
 * Class DishTagController
 * @package App\Http\Controllers\API
 * @group DishTag
 */
class DishTagController extends Controller
{

    /**
     * @param DishTagListRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(DishTagListRequest $request)
    {
        $dishTags = DishTag::query();
        if ($request->has('name')) {
            $dishTags = $dishTags->where('name', 'like', '%' . $request->name . '%');
        }
        $dishTags = $dishTags->paginate();
        $collection = DishTagResource::collection($dishTags);
        return $collection;
    }

    /**
     * @param DishTagRequest $request
     * @return DishTagResource
     */
    public function create(DishTagRequest $request)

    {
        $dishTag = new DishTag($request->input());
        $dishTag->save();
        $resource = new DishTagResource($dishTag);
        return $resource;
    }

    /**
     * @param DishTagRequest $request
     * @param $id
     * @return DishTagResource
     */
    public function update(DishTagRequest $request, $id)
    {
        $dishTag = DishTag::query()->where(['id' => $id])->first();
        if (empty($dishTag)) {
            throw new NotFoundHttpException("Тег блюда номер $id не найден.");
        }
        $dishTag->fill($request->input());
        $dishTag->save();
        $resource = new DishTagResource($dishTag);
        return $resource;
    }

}
