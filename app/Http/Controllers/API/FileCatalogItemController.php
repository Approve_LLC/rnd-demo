<?php

namespace App\Http\Controllers\API;

use App\DownloadContext;
use App\Http\Resources\FileCatalogItemResource;
use App\FileCatalogItem;
use App\Services\FileManagerService\FileManagerService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class FileCatalogItemController
 * @package App\Http\Controllers\API
 */
class FileCatalogItemController extends Controller
{

    /**
     * @var FileManagerService
     */
    private $fileManagerService;

    public function __construct(FileManagerService $fileManagerService)
    {
        $this->fileManagerService = $fileManagerService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'parentId' => 'string'
        ]);
        if(!empty( $request->parentId)) {
            $fileCatalogItems = FileCatalogItem::where(['parent_id' => $request->parentId])->paginate();
        }
        else {
            $fileCatalogItems = FileCatalogItem::paginate();
        }
        $collection = FileCatalogItemResource::collection($fileCatalogItems);
        return $collection;
    }

    /**
     * @param $id
     * @return FileCatalogItemResource
     */
    public function show($id)
    {
        $fileCatalogItem = FileCatalogItem::where(['id' => $id])->first();
        $resource = new FileCatalogItemResource($fileCatalogItem);
        return $resource;
    }

    /**
     * @param Request $request
     * @return FileCatalogItemResource
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'parentId' => 'required|string',
            'name' => 'required|string'
        ]);
        $fileCatalogItem = new FileCatalogItem([
            'parent_id' => $request->parentId,
            'is_folder' => $request->isFolder??false,
            'name' => $request->name

        ]);
        $fileCatalogItem->save();
        $resource = new FileCatalogItemResource($fileCatalogItem);
        return $resource;
    }

    /**
     * @param Request $request
     * @param $id
     * @return FileCatalogItemResource
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'string'
        ]);
        $fileCatalogItem = FileCatalogItem::where(['id' => $id])->first();
        $fileCatalogItem->name = $request->name;
        $fileCatalogItem->save();
        $resource = new FileCatalogItemResource($fileCatalogItem);
        return $resource;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        FileCatalogItem::where(['id' => $id])->delete();
        return response()->json([
            'data' => 'Deleted!'
        ], 200);
    }

    /**
     * @param Request $request
     * @param $id
     * @throws \Illuminate\Validation\ValidationException
     */
    public function uploadImage(Request $request, $id)
    {
        $this->validate($request, [
            'fileUrl' => 'required|string',
            'name' => 'required|string',
            'description' => 'string',
        ]);
        $context = new DownloadContext($this->fileManagerService);
        $data = [
            'name' => $request->name,
            'description' => $request->description,
        ];
        $bucket = $context->bucket;
        $context->getFileManagerService()->updateEntity($id, $data, $request->fileUrl, $bucket);
    }

}
