<?php

namespace App\Http\Requests\Dish;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DishRequest
 * @package App\Http\Requests\Dish
 */
class DishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'tags' => 'integer'
        ];
    }
}
