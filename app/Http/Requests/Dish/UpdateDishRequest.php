<?php

namespace App\Http\Requests\Dish;

use App\Dish;
use Illuminate\Foundation\Http\FormRequest;

class UpdateDishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'tip_text' => 'string',
            'shelf_life' => 'integer',
            'net_weight' => 'integer',
            'cooking_time' => 'integer',
            'difficulty' => 'string|in:' . Dish::DIFFICULTY_EASY . ',' . Dish::DIFFICULTY_MEDIUM . ',' . Dish::DIFFICULTY_DIFFICULT,
            'protein' => 'integer',
            'fat' => 'integer',
            'carb' => 'integer',
            'calories' => 'integer',
            'currentRecipe.steps' => 'array',
            'currentRecipe.steps.description' => 'string',
            'currentRecipe.steps.duration' => 'integer',
            'currentRecipe.steps.step_number' => 'integer',
            'currentRecipe.steps.image.file' => 'file|image',
            'currentRecipe.steps.image.id' => 'string|required_with:currentRecipe.steps.image.file',
            'currentRecipe.steps.image.name' => 'string|required_with:currentRecipe.steps.image.file',
            'currentRecipe.steps.image.description' => 'string|required_with:currentRecipe.steps.image.file',
            'inventory' => 'array',
            'inventory.name' => 'string',
            'tags' => 'array',
            'tags.name' => 'string',
            'tags.is_shown' => 'boolean',
            'tags.is_used_for_filtering' => 'boolean',
            'tags.is_used_for_sorting' => 'boolean',
            'file' => 'file'
        ];
    }
}
