<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DishTagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'is_shown' => 'boolean',
            'is_used_for_filtering' => 'boolean',
            'is_used_for_sorting' => 'boolean'
        ];
    }
}
