<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DishResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'recipe' =>  new RecipeResource($this->currentRecipe),
            'ext_id' => $this->ext_id,
            'name' => $this->name,
            'tip_text' => $this->tip_text,
            'tipChef' => new ChefResource($this->tipChef),
            'shelf_life' => $this->shelf_life,
            'net_weight' => $this->net_weight,
            'cooking_time' => $this->net_weight,
            'difficulty' => $this->difficulty,
            'protein' => $this->protein,
            'fat' => $this->fat,
            'carb' => $this->carb,
            'calories' => $this->calories,
            'inventory' => InventoryResource::collection($this->inventory),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}