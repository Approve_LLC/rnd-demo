<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class RecipeResource
 * @package App\Http\Resources
 */
class RecipeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'steps' => RecipeStepResource::collection($this->steps),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
