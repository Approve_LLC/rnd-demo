<?php

namespace App\Services\DownloadService;

use App\Services\FileManagerService\FileManagerService;
use Illuminate\Support\ServiceProvider;

class DownloadServiceProvider extends ServiceProvider
{

    private $fileManagerService;

    public function boot(FileManagerService $fileManagerService)
    {
        $this->fileManagerService = $fileManagerService;
    }

    public function register()
    {
        $this->app->singleton(DownloadService::class, function ($app) {
            return new DownloadService($this->fileManagerService);
        });
    }
}