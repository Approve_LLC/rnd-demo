<?php

namespace App\Services\DownloadService;

use App\DownloadContext;
use App\Interfaces\DownloadInterface;
use App\Services\FileManagerService\FileManagerService;

class DownloadService
{
    const CHEFS = 'chefs';
    const NOMENCLATURE = 'nomenclature';
    const INVENTORIES = 'inventories';
    const PROMOTION_SETS = 'promotion_sets';
    const DISHES = 'dishes';
    const DISH_TAGS = 'dish_tags';
    const DISH_PICTURES = 'dish_pictures';

    /**
     * @var FileManagerService
     */
    private $fileManagerService;

    public function __construct(FileManagerService $fileManagerService)
    {
        $this->fileManagerService = $fileManagerService;
    }

    /**
     * @param string $modelId
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function run($modelId = self::CHEFS)
    {
        $context = new DownloadContext($this->fileManagerService);

        $client = new \GuzzleHttp\Client();
        switch ($modelId) {
            case self::CHEFS:
                $endpoint = config('app.chef_domain') . '/api/v1/cooks';
                $class = 'App\Chef';
                break;
            case self::NOMENCLATURE:
                $endpoint = config('app.chef_domain') . '/api/v1/nomenclature';
                $class = 'App\Nomenclature';
                break;
            case self::INVENTORIES:
                $endpoint = config('app.raisa_domain') . '/v1/sync/tools';
                $class = 'App\Inventory';
                break;
            case self::PROMOTION_SETS:
                $endpoint = config('app.raisa_domain') . '/v1/sync/promotion-set-images';
                $class = 'App\PromotionSet';
                break;
            case self::DISHES:
                $endpoint = config('app.raisa_domain') . '/v1/dishes';
                $class = 'App\Dish';
                break;
            case self::DISH_TAGS:
                $endpoint = config('app.raisa_domain') . '/v1/dish-tags';
                $class = 'App\DishTag';
                break;
            case self::DISH_PICTURES:
                $endpoint = config('app.raisa_domain') . '/v1/dish-pictures';
                $class = 'App\DishPicture';
                break;
            default:
                $endpoint = config('app.chef_domain') . '/api/v1/cooks';
                $class = 'App\Chef';
                break;
        }
        $response = $client->request('GET', $endpoint);
        $content = json_decode($response->getBody()->getContents());
        foreach ($content as $data) {
            /** @var DownloadInterface $model */
            $model = $class::firstOrNew(['ext_id' => $data->id]);
            $model->downloadContent($data, $context);
        }
    }
}
