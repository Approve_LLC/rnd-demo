<?php

namespace App\Services\FileManagerService;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\EntityFile;
use App\EntityFileVersion;

/**
 * Class FileManagerService
 * @package App\Services\FileManagerService
 */
class FileManagerService
{
    /**
     * @param $model
     * @param $data
     * @param $fileUrl
     * @param $bucket
     * @return EntityFile
     */
    public function downloadEntityFileVersion($model, $data, $fileUrl, $bucket)
    {
        $entityFile = EntityFile::firstOrNew(['entity_id' => $model->id]);
        $entityFile->entity()->associate($model);
        $entityFile->save();

        $entityFileVersion = new EntityFileVersion();
        $uuid = Str::uuid();
        $entityFileVersion->id = $uuid;
        $entityFileVersion->name = $data->name;
        $entityFileVersion->file()->associate($entityFile);
        $entityFileVersion->description = $data->description;
        $entityFileVersion->author_id = null;
        $entityFileVersion->file_name = basename($fileUrl);
        $entityFileVersion->bucket()->associate($bucket);
        $key = $entityFile->entity->getStorageKey();
        $entityFileVersion->key = $key . '/' . $model->id . '/' . $entityFileVersion->id . '/' . $entityFileVersion->file_name;
        $entityFileVersion->save();

        $entityFile->entity_file_version_id = $uuid;
        $entityFile->save();

        return $entityFile;
    }

    /**
     * @param $id
     * @param $model
     * @param $fileUrl
     * @param $bucket
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function updateEntity($id, $model, $fileUrl, $bucket)
    {
        $entityFile = EntityFile::query()->where(['id' => $id])->first();
        $entityFile->entity()->associate($model);
        $entityFile->save();

        $entityFileVersion = new EntityFileVersion();
        $uuid = Str::uuid();
        $entityFileVersion->id = $uuid;
        $entityFileVersion->name = $model->name;
        $entityFileVersion->file()->associate($entityFile);
        $entityFileVersion->description = $model->description;
        $entityFileVersion->author_id = null;
        $entityFileVersion->file_name = basename($fileUrl);
        $entityFileVersion->bucket()->associate($bucket);
        $key = $entityFile->entity->getStorageKey();
        $entityFileVersion->key = $key . '/' . $id . '/' . $entityFileVersion->id . '/' . $entityFileVersion->file_name;
        $entityFileVersion->save();

        $entityFile->entity_file_version_id = $uuid;
        $entityFile->save();

        return $entityFile;
    }

    public function uploadFile($file, $key)
    {
        try {
            $image = file_get_contents($file);
            $imageName = $key;
            Storage::cloud()->put($imageName, $image);
        } catch (\Exception $e) {
            echo 'Could not load image.' . PHP_EOL;
        }
    }

    /**
     * @param $fileUrl
     * @param $key
     */
    public function uploadFileByUrl($fileUrl, $key)
    {
        try {
            $image = file_get_contents($fileUrl);
            $imageName = $key;
            Storage::cloud()->put($imageName, $image);
        } catch (\Exception $e) {
            echo 'Could not load image.' . PHP_EOL;
        }
    }

}