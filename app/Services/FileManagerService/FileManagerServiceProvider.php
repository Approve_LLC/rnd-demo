<?php

namespace App\Services\FileManagerService;

use Illuminate\Support\ServiceProvider;

class FileManagerServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->singleton(FileManagerService::class, function ($app) {
            return new FileManagerService();
        });
    }
}