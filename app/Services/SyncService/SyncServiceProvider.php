<?php

namespace App\Services\SyncService;

use Illuminate\Support\ServiceProvider;

class SyncServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->singleton(SyncService::class, function ($app) {
            return new SyncService();
        });
    }
}