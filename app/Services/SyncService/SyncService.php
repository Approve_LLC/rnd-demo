<?php

namespace App\Services\SyncService;

use App\Interfaces\SyncInterface;
use App\MenuGroup;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SyncService
 * @package App\Services\SyncService
 */
class SyncService
{
    const CHEFS = 'chefs';
    const NOMENCLATURE = 'nomenclature';
    const NOMENCLATURE_GROUP = 'nomenclature_group';
    const DISHES = 'dishes';
    const MENU_DISH = 'menu_dish';
    const PROMOTION_SETS = 'promotion_sets';
    const MENU_GROUP = 'menu_group';
    const MENU_TYPE = 'menu_type';
    const PACKED_PRODUCT = 'packed_product';
    const PACKED_PRODUCT_ITEM = 'packed_product_item';
    const MEASURE_UNIT = 'measure_unit';

    /**
     * @param $content
     * @param $class
     * @return array
     */
    private function makeDictionary($content, $class)
    {
        $dictionary = [];
        foreach ($content as $data) {
            $dictionary[$data->id]['external'] = $data;
        }
        $models = $class::all(['ext_id']);
        foreach ($models as $model) {
            $dictionary[$model->ext_id]['local'] = $model;
        }

        return $dictionary;
    }

    /**
     * @param string $modelId
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function run($modelId = self::MENU_TYPE)
    {
        $client = new \GuzzleHttp\Client();

        $afterLoad = null;

        switch ($modelId) {
            case self::NOMENCLATURE:
                $endpoint = config('app.raisa_domain') . '/v1/sync/nomenclature';
                $class = 'App\Nomenclature';
                break;
            case self::NOMENCLATURE_GROUP:
                $endpoint = config('app.raisa_domain') . '/v1/sync/nomenclature-group';
                $class = 'App\NomenclatureGroup';
                break;
            case self::DISHES:
                $endpoint = config('app.raisa_domain') . '/v1/dishes';
                $class = 'App\Dish';
                break;
            case self::MENU_DISH:
                $endpoint = config('app.raisa_domain') . '/v1/sync/menu';
                $class = 'App\MenuDish';
                break;
            case self::MEASURE_UNIT:
                $endpoint = config('app.raisa_domain') . '/v1/sync/measure-unit';
                $class = 'App\MeasureUnit';
                break;
            case self::PROMOTION_SETS:
                $endpoint = config('app.raisa_domain') . '/v1/sync/promotion-sets';
                $class = 'App\PromotionSet';
                break;
            case self::MENU_GROUP:
                $endpoint = config('app.raisa_domain') . '/v1/sync/menu-groups';
                $class = 'App\MenuGroup';
                $afterLoad = function($dictionary) {
                    foreach($dictionary as $item) {
                        $external = $item['external'];
                        $parent = MenuGroup::query()->where(['ext_id' => $external->parent_id])->first();
                        $model = $item['local'];
                        $model->parent()->associate($parent);
                        $model->save();
                    }
                };
                break;
            case self::MENU_TYPE:
                $endpoint = config('app.raisa_domain') . '/v1/sync/menu-types';
                $class = 'App\MenuType';
                break;
            case self::PACKED_PRODUCT:
                $endpoint = config('app.raisa_domain') . '/v1/sync/packings';
                $class = 'App\PackedProduct';
                break;
            case self::PACKED_PRODUCT_ITEM:
                $endpoint = config('app.raisa_domain') . '/v1/sync/packing-items';
                $class = 'App\PackedProductItem';
                break;
            default:
                $endpoint = config('app.raisa_domain') . '/v1/sync/menu-types';
                $class = 'App\MenuType';
                break;
        }
        $response = $client->request('GET', $endpoint);
        $content = json_decode($response->getBody()->getContents());
        $dictionary = $this->makeDictionary($content, $class);
        foreach ($dictionary as $key => $value) {
            /** @var SyncInterface | Model $model */
            $model = $class::firstOrNew(['ext_id' => $key]);
            $result = isset($value['external']) || !isset($value['local']);
            if ($result) {
                $model->setSyncContent($value['external']);
                //почему-то при = $model ничего не работает...
                $dictionary[$key]['local'] = $class::firstOrNew(['ext_id' => $key]);
            } else {
                $model->delete();
            }
        }

        if ($afterLoad) {
            $afterLoad($dictionary);
        }
    }
}