<?php

namespace App;

use App\Interfaces\SyncInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\MenuGroup
 *
 * @property string $id
 * @property string $parent_id
 * @property int $ext_id
 * @property string $name
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property MenuGroup $menuGroup
 * @property MenuDish[] $menuDishes
 * @property \App\MenuGroup $parent
 */
class MenuGroup extends Model implements SyncInterface
{
    use SoftDeletes;

    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['parent_id', 'ext_id', 'name', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\MenuGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menuDishes()
    {
        return $this->hasMany('App\MenuDish');
    }

    /**
     * @param $data
     * @return null
     */
    public function setSyncContent($data)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        $this->save();
    }
}

