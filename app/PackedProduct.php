<?php

namespace App;

use App\Interfaces\SyncInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\PackedProduct
 *
 * @property string $id
 * @property int $ext_id
 * @property string $name
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property MenuDishComponent[] $menuDishComponents
 * @property PackedProductItem[] $packedProductItems
 * @property \Illuminate\Database\Eloquent\Collection|\App\PackedProductItem[] $items
 */
class PackedProduct extends Model implements SyncInterface
{
    use SoftDeletes;

    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['ext_id', 'name', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\PackedProductItem');
    }

    /**
     * @param $data
     * @return null
     */
    public function setSyncContent($data)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        $this->save();
    }
}
