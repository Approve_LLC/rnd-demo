<?php

namespace App;

use App\Interfaces\FileManagerInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LaravelFillableRelations\Eloquent\Concerns\HasFillableRelations;

/**
 * @property string $id
 * @property string $recipe_id
 * @property int $step_number
 * @property string $description
 * @property int $duration
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property Recipe $recipe
 * @property \App\EntityFile $picture
 */
class RecipeStep extends Model implements FileManagerInterface
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['recipe_id', 'step_number', 'description', 'duration', 'deleted_at', 'created_at', 'updated_at'];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipe()
    {
        return $this->belongsTo('App\Recipe', 'recipe_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function picture()
    {
        return $this->morphOne('App\EntityFile', 'entity');
    }

    /**
     * @return string
     */
    public function getStorageKey()
    {
        return 'recipe-step';
    }
}
