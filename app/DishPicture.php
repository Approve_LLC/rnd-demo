<?php

namespace App;

use App\Interfaces\DownloadInterface;
use App\Interfaces\FileManagerInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\DishPicture
 *
 * @property string $id
 * @property string $dish_id
 * @property string $angle
 * @property string $destination
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property Dish $dish
 * @property \App\EntityFile $file
 */
class DishPicture extends Model implements DownloadInterface, FileManagerInterface
{

    use SoftDeletes;

    const ANGLE_TOP = 0;
    const ANGLE_SIDE = 1;

    const DESTINATION_FULL = 0;
    const DESTINATION_MAKE_YOURSELF = 0;
    const DESTINATION_FOR_APPLICATION = 1;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['dish_id', 'angle', 'destination', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dish()
    {
        return $this->belongsTo('App\Dish');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function file()
    {
        return $this->morphOne('App\EntityFile', 'entity');
    }

    /**
     * @param mixed $data
     * @param DownloadContext $context
     */
    public function downloadContent($data, DownloadContext $context)
    {
        $dish = Dish::where(['ext_id' => $data->dish_id])->first();
        $this->dish()->associate($dish);
        $this->angle = $data->angle;
        $this->destination = $data->destination;
        $this->save();
        $dish = Dish::where(['chef_id' => $this->cook->id]);
        $dish->tipChef()->associate($this);
        $this->save();
        if (!$this->file()->exists()) {
            $bucket = $context->bucket;
            $entityFile = $context
                ->getFileManagerService()
                ->downloadEntityFileVersion($this, $data, $data->image_url, $bucket);
            $key = $entityFile->current->key;
            $context->getFileManagerService()->uploadFileByUrl($data->image_url, $key);
            $this->file()->save($entityFile);
            $this->save();
        }
    }

    /**
     * @return string
     */
    public function getStorageKey()
    {
        return 'dish-picture';
    }
}
