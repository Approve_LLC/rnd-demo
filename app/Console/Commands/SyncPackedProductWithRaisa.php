<?php

namespace App\Console\Commands;

use App\Services\SyncService\SyncService;
use Illuminate\Console\Command;

/**
 * Class SyncPackedProductWithRaisa
 * @package App\Console\Commands
 */
class SyncPackedProductWithRaisa extends Command
{

    protected $signature = 'sync:packed-product';
    protected $description = 'Sync packed product with Raisa';

    private $syncService;

    public function __construct(SyncService $syncService)
    {
        parent::__construct();
        $this->syncService = $syncService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncService->run(SyncService::PACKED_PRODUCT);
        $this->info('Success!');
    }
}
