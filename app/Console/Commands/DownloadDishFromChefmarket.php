<?php

namespace App\Console\Commands;

use App\Services\DownloadService\DownloadService;
use Illuminate\Console\Command;

/**
 * Class DownloadDishFromChefmarket
 * @package App\Console\Commands
 */
class DownloadDishFromChefmarket extends Command
{

    protected $signature = 'download:dish';
    protected $description = 'Здесь происходит импорт блюд с рецептами, инвентарем для блюд';

    private $downloadService;

    public function __construct(DownloadService $downloadService)
    {
        parent::__construct();
        $this->downloadService = $downloadService;
    }

    public function handle()
    {
        $this->downloadService->run(DownloadService::DISHES);
        $this->info('Success!');
    }
}
