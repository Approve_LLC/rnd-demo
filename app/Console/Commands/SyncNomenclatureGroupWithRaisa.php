<?php

namespace App\Console\Commands;

use App\Services\SyncService\SyncService;
use Illuminate\Console\Command;

/**
 * Class SyncNomenclatureGroupWithRaisa
 * @package App\Console\Commands
 */
class SyncNomenclatureGroupWithRaisa extends Command
{

    protected $signature = 'sync:nomenclature-group';
    protected $description = 'Sync nomenclature group with Raisa';

    private $syncService;

    public function __construct(SyncService $syncService)
    {
        parent::__construct();
        $this->syncService = $syncService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncService->run(SyncService::NOMENCLATURE_GROUP);
        $this->info('Success!');
    }
}
