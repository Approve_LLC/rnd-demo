<?php

namespace App\Console\Commands;

use App\Services\SyncService\SyncService;
use Illuminate\Console\Command;

/**
 * Class SyncPackedProductItemWithRaisa
 * @package App\Console\Commands
 */
class SyncPackedProductItemWithRaisa extends Command
{

    protected $signature = 'sync:packed-product-item';
    protected $description = 'Sync packed product item with Raisa';

    private $syncService;

    public function __construct(SyncService $syncService)
    {
        parent::__construct();
        $this->syncService = $syncService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncService->run(SyncService::PACKED_PRODUCT_ITEM);
        $this->info('Success!');
    }
}
