<?php

namespace App\Console\Commands;

use App\Services\SyncService\SyncService;
use Illuminate\Console\Command;

/**
 * Class SyncNomenclatureWithRaisa
 * @package App\Console\Commands
 */
class SyncNomenclatureWithRaisa extends Command
{

    protected $signature = 'sync:nomenclature';
    protected $description = 'Sync nomenclature with Raisa';

    private $syncService;

    public function __construct(SyncService $syncService)
    {
        parent::__construct();
        $this->syncService = $syncService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncService->run(SyncService::NOMENCLATURE);
        $this->info('Success!');
    }
}
