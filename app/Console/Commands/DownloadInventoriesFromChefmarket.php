<?php

namespace App\Console\Commands;

use App\Services\DownloadService\DownloadService;
use Illuminate\Console\Command;

class DownloadInventoriesFromChefmarket extends Command
{

    protected $signature = 'download:inventories';
    protected $description = 'Download inventories from Chefmarket';

    private $downloadService;

    public function __construct(DownloadService $downloadService)
    {
        parent::__construct();
        $this->downloadService = $downloadService;
    }

    public function handle()
    {
        $this->downloadService->run(DownloadService::INVENTORIES);
        $this->info('Success!');
    }
}
