<?php

namespace App\Console\Commands;

use App\Services\SyncService\SyncService;
use Illuminate\Console\Command;

/**
 * Class SyncDishWithRaisa
 * @package App\Console\Commands
 */
class SyncDishWithRaisa extends Command
{

    protected $signature = 'sync:dish';
    protected $description = 'Sync dish with Raisa';

    private $syncService;

    public function __construct(SyncService $syncService)
    {
        parent::__construct();
        $this->syncService = $syncService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncService->run(SyncService::DISHES);
        $this->info('Success!');
    }
}
