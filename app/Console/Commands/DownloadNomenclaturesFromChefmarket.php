<?php

namespace App\Console\Commands;

use App\Services\DownloadService\DownloadService;
use Illuminate\Console\Command;

class DownloadNomenclaturesFromChefmarket extends Command
{

    protected $signature = 'download:nomenclature';
    protected $description = 'Download nomenclature from Chefmarket';

    private $downloadService;

    public function __construct(DownloadService $downloadService)
    {
        parent::__construct();
        $this->downloadService = $downloadService;
    }

    public function handle()
    {
        $this->downloadService->run(DownloadService::NOMENCLATURE);
        $this->info('Success!');
    }
}
