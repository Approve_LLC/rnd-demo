<?php

namespace App\Console\Commands;

use App\MenuGroup;
use App\Services\SyncService\SyncService;
use Illuminate\Console\Command;

/**
 * Class SyncMenuGroupWithRaisa
 * @package App\Console\Commands
 */
class SyncMenuGroupWithRaisa extends Command
{

    protected $signature = 'sync:menu-group';
    protected $description = 'Sync menu group with Raisa';

    private $syncService;

    public function __construct(SyncService $syncService)
    {
        parent::__construct();
        $this->syncService = $syncService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncService->run(SyncService::MENU_GROUP);
        $this->info('Success!');
    }
}
