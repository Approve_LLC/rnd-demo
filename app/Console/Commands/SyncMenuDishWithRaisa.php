<?php

namespace App\Console\Commands;

use App\Services\SyncService\SyncService;
use Illuminate\Console\Command;

/**
 * Class SyncMenuDishWithRaisa
 * @package App\Console\Commands
 */
class SyncMenuDishWithRaisa extends Command
{

    protected $signature = 'sync:menu-dish';
    protected $description = 'Sync menu dish with Raisa';

    private $syncService;

    public function __construct(SyncService $syncService)
    {
        parent::__construct();
        $this->syncService = $syncService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncService->run(SyncService::MENU_DISH);
        $this->info('Success!');
    }
}
