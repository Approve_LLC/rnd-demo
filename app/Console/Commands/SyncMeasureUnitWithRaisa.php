<?php

namespace App\Console\Commands;

use App\Services\SyncService\SyncService;
use Illuminate\Console\Command;

/**
 * Class SyncMeasureUnitWithRaisa
 * @package App\Console\Commands
 */
class SyncMeasureUnitWithRaisa extends Command
{

    protected $signature = 'sync:measure-unit';
    protected $description = 'Sync measure unit with Raisa';

    private $syncService;

    public function __construct(SyncService $syncService)
    {
        parent::__construct();
        $this->syncService = $syncService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncService->run(SyncService::MEASURE_UNIT);
        $this->info('Success!');
    }
}
