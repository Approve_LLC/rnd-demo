<?php

namespace App\Console\Commands;

use App\Services\DownloadService\DownloadService;
use Illuminate\Console\Command;

class DownloadDishTagsFromChefmarket extends Command
{

    protected $signature = 'download:dish-tags';
    protected $description = 'Download dish tags from Chefmarket';

    private $downloadService;

    public function __construct(DownloadService $downloadService)
    {
        parent::__construct();
        $this->downloadService = $downloadService;
    }

    public function handle()
    {
        $this->downloadService->run(DownloadService::DISH_TAGS);
        $this->info('Success!');
    }
}
