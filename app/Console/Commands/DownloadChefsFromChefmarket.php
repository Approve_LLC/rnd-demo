<?php

namespace App\Console\Commands;

use App\Services\DownloadService\DownloadService;
use Illuminate\Console\Command;

class DownloadChefsFromChefmarket extends Command
{

    protected $signature = 'download:chefs';
    protected $description = 'Download chefs from Chefmarket';

    private $downloadService;

    public function __construct(DownloadService $downloadService)
    {
        parent::__construct();
        $this->downloadService = $downloadService;
    }

    public function handle()
    {
        $this->downloadService->run(DownloadService::CHEFS);
        $this->info('Success!');
    }
}
