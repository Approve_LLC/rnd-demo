<?php

namespace App\Console\Commands;

use App\Services\DownloadService\DownloadService;
use Illuminate\Console\Command;

/**
 * Class DownloadPromotionSetFromRaisa
 * @package App\Console\Commands
 */
class DownloadPromotionSetFromRaisa extends Command
{

    protected $signature = 'download:promotion-set';
    protected $description = 'Download promotion set from Chefmarket';

    private $downloadService;

    public function __construct(DownloadService $downloadService)
    {
        parent::__construct();
        $this->downloadService = $downloadService;
    }

    public function handle()
    {
        $this->downloadService->run(DownloadService::PROMOTION_SETS);
        $this->info('Success!');
    }
}
