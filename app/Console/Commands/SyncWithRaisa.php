<?php

namespace App\Console\Commands;

use App\Services\SyncService\SyncService;
use Illuminate\Console\Command;

class SyncWithRaisa extends Command
{

    protected $signature = 'sync:raisa {modelId}';
    protected $description = 'Sync with Raisa';

    private $syncService;

    public function __construct(SyncService $syncService)
    {
        parent::__construct();
        $this->syncService = $syncService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncService->run($this->argument('modelId'));
        $this->info('Success!');
    }
}
