<?php

namespace App\Console\Commands;

use App\Services\SyncService\SyncService;
use Illuminate\Console\Command;

/**
 * Class SyncMenuTypeWithRaisa
 * @package App\Console\Commands
 */
class SyncMenuTypeWithRaisa extends Command
{

    protected $signature = 'sync:menu-type';
    protected $description = 'Sync menu type with Raisa';

    private $syncService;

    public function __construct(SyncService $syncService)
    {
        parent::__construct();
        $this->syncService = $syncService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncService->run(SyncService::MENU_TYPE);
        $this->info('Success!');
    }
}
