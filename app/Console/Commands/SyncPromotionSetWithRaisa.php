<?php

namespace App\Console\Commands;

use App\Services\SyncService\SyncService;
use Illuminate\Console\Command;

/**
 * Class SyncPromotionSetWithRaisa
 * @package App\Console\Commands
 */
class SyncPromotionSetWithRaisa extends Command
{

    protected $signature = 'sync:promotion-set';
    protected $description = 'Sync promotion set with Raisa';

    private $syncService;

    public function __construct(SyncService $syncService)
    {
        parent::__construct();
        $this->syncService = $syncService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->syncService->run(SyncService::PROMOTION_SETS);
        $this->info('Success!');
    }
}
