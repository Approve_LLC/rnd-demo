<?php

namespace App\Console\Commands;

use App\Services\DownloadService\DownloadService;
use Illuminate\Console\Command;

/**
 * Class DownloadDishPicture
 * @package App\Console\Commands
 */
class DownloadDishPicture extends Command
{

    protected $signature = 'download:dish-picture';
    protected $description = 'Здесь происходит импорт изображений блюд';

    private $downloadService;

    public function __construct(DownloadService $downloadService)
    {
        parent::__construct();
        $this->downloadService = $downloadService;
    }

    public function handle()
    {
        $this->downloadService->run(DownloadService::DISH_PICTURES);
        $this->info('Success!');
    }
}
