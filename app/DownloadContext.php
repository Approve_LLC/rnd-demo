<?php


namespace App;


use App\Services\FileManagerService\FileManagerService;

class DownloadContext
{
    public $bucket;

    /**
     * @var FileManagerService
     */
    private $fileManagerService;

    public function __construct(FileManagerService $fileManagerService)
    {
        $this->fileManagerService = $fileManagerService;
        $this->bucket = ObjectStorageBucket::firstOrNew([
            'bucket' => config('app.bucket_rnd_name'),
            'host' => config('app.minio_endpoint')
        ]);
    }

    /**
     * @return FileManagerService
     */
    public function getFileManagerService(): FileManagerService
    {
        return $this->fileManagerService;
    }
}