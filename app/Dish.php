<?php

namespace App;

use App\Interfaces\DownloadInterface;
use App\Interfaces\FileManagerInterface;
use App\Interfaces\SyncInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use stdClass;

/**
 * App\Dish
 *
 * @property string $id
 * @property string $recipe_id
 * @property int $ext_id
 * @property string $name
 * @property string $tip_text
 * @property string $chef_id
 * @property int $shelf_life
 * @property int $net_weight
 * @property int $cooking_time
 * @property string $difficulty
 * @property int $protein
 * @property int $fat
 * @property int $carb
 * @property int $calories
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property Recipe $currentRecipe
 * @property Recipe[] $recipes
 * @property MenuDish[] $menuDishes
 * @property \Illuminate\Database\Eloquent\Model|\Eloquent $entity
 * @property \Illuminate\Database\Eloquent\Collection|\App\Inventory[] $inventory
 * @property \App\Chef $tipChef
 */
class Dish extends Model implements SyncInterface, DownloadInterface, FileManagerInterface
{
    use SoftDeletes;

    const DIFFICULTY_EASY = 1;
    const DIFFICULTY_MEDIUM = 2;
    const DIFFICULTY_DIFFICULT = 3;

    protected $dates = ['deleted_at'];
    protected $keyType = 'string';
    protected $fillable = ['recipe_id', 'ext_id', 'name', 'tip_text', 'chef_id', 'shelf_life', 'net_weight', 'cooking_time', 'difficulty', 'protein', 'fat', 'carb', 'calories', 'deleted_at', 'created_at', 'updated_at'];
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    public $incrementing = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currentRecipe()
    {
        return $this->belongsTo('App\Recipe', 'recipe_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recipes()
    {
        return $this->hasMany('App\Recipe');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function inventory()
    {
        return $this->belongsToMany('App\Inventory');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menuDishes()
    {
        return $this->hasMany('App\MenuDish');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipChef()
    {
        return $this->belongsTo('App\Chef', 'chef_id');
    }

    //у нас многие ко многим же или hasMany?
    public function tags()
    {
        return $this->belongsToMany('App\DishTag');
    }

    /**
     * @param mixed $data
     * @param DownloadContext $context
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function downloadContent($data, DownloadContext $context)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        $this->net_weight = $data->weight;
        $this->cooking_time = $data->cook_time;
        $this->difficulty = $data->cook_complexity;
        $this->protein = $data->nutrition_facts->protein;
        $this->fat = $data->nutrition_facts->fat;
        $this->carb = $data->nutrition_facts->carbohydrate;
        $this->calories = $data->nutrition_facts->calories;
        //$chef = Chef::where(['ext_id' => $data->chef_id])->first();
        //$this->tipChef()->associate($chef);
        //$this->tip_text = $data->tip_text;
        $client = new \GuzzleHttp\Client();
        //Тк получение рецептов блюда в апи имеется только у запроса отдельной сущности,
        //то на приходится повторно обращаться к апи при импорте, чтобы получить рецепт какого-то блюда.
        //то же относится и к инвеннтарю
        $endpoint = config('app.raisa_domain') . '/v1/dishes/' . $data->id;
        $response = $client->request('GET', $endpoint);
        $content = json_decode($response->getBody()->getContents());
        if (!empty($content)) {
            foreach ($content->tools as $tool) {
                $inventoryItem = Inventory::query()->where(['ext_id' => $tool->id])->first();
                $this->inventory()->attach($inventoryItem);
            }
            $recipe = new Recipe();
            $uuid = Str::uuid();
            $recipe->id = $uuid;
            $recipe->dish()->associate($this);
            $recipe->save();
            if (!empty($content->receipt)) {
                foreach ($content->receipt as $item) {
                    $recipeStep = new RecipeStep();
                    $recipeStep->recipe_id = $recipe->id;
                    $recipeStep->step_number = $item->step;
                    $recipeStep->description = $item->about;
                    //такого параметра нет в раисе. оставляю пустые.
                    $recipeStep->duration = 0;//$item->duration;
                    $recipeStep->save();
                    if (!$recipeStep->picture()->exists()) {
                        $bucket = $context->bucket;
                        $data = new stdClass;
                        $data->name = $item->title;
                        $data->description = $item->about;
                        //какой урл у них?
                        $imgUrl = config('app.raisa_domain') . '/' . $item->image;
                        $entityFile = $context
                            ->getFileManagerService()
                            ->downloadEntityFileVersion($this, $data, $imgUrl, $bucket);
                        $key = $entityFile->current->key;
                        $context->getFileManagerService()->uploadFileByUrl($imgUrl, $key);
                        $recipeStep->picture()->save($entityFile);
                        $recipeStep->save();
                    }
                }
            }
            $this->currentRecipe()->associate($recipe);
        }
        $this->save();
    }

    /**
     * @param $data
     */
    public function setSyncContent($data)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        $this->net_weight = $data->weight;
        $this->cooking_time = $data->cook_time;
        $this->difficulty = $data->cook_complexity;
        $this->protein = $data->nutrition_facts->protein;
        $this->fat = $data->nutrition_facts->fat;
        $this->carb = $data->nutrition_facts->carbohydrate;
        $this->calories = $data->nutrition_facts->calories;
        $this->save();
    }

    /**
     * @return string
     */
    public function getStorageKey()
    {
        return 'dish';
    }

}


