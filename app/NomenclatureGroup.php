<?php

namespace App;

use App\Interfaces\SyncInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\NomenclatureGroup
 *
 * @property string $id
 * @property int $ext_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 */
class NomenclatureGroup extends Model implements SyncInterface
{
    use SoftDeletes;

    const NOMENCLATURE_GROUP_PACKING_ID_FIRST = 19;
    const NOMENCLATURE_GROUP_PACKING_ID_SECOND = 21;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';
    
    public $incrementing = true;

    public function setSyncContent($data)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        $this->save();
    }

}
