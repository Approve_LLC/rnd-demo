<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\ObjectStorageBucket
 *
 * @property string $id
 * @property string $host
 * @property string $bucket
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class ObjectStorageBucket extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';
    public $incrementing = true;
}
