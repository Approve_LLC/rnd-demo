<?php

namespace App;

use App\Interfaces\DownloadInterface;
use App\Interfaces\SyncInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\MeasureUnit
 *
 * @property string $id
 * @property int $ext_id
 * @property string $name
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class MeasureUnit extends Model implements SyncInterface
{

    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['ext_id', 'name', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return string
     */
    public function getStorageKey()
    {
        return 'measure-unit';
    }

    /**
     * @param $data
     * @return null
     */
    public function setSyncContent($data)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        $this->save();
    }
}
