<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Recipe
 *
 * @property string $id
 * @property string $dish_id
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property Dish $dish
 * @property Dish[] $dishes
 */
class Recipe extends Model
{
    use SoftDeletes;

    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['dish_id', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dish()
    {
        return $this->belongsTo('App\Dish');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function steps()
    {
        return $this->hasMany('App\RecipeStep');
    }
}
