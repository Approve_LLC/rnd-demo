<?php

namespace App\Interfaces;

use App\DownloadContext;

/**
 * Interface DownloadInterface
 * @package App\Interfaces
 */
interface DownloadInterface
{
    /**
     * @param $data mixed Data being downloaded for the entity
     * @param DownloadContext $context The download context
     */
    public function downloadContent($data, DownloadContext $context);
}
