<?php

namespace App\Interfaces;

/**
 * Interface FileManagerInterface
 * @package App\Interfaces
 */
interface FileManagerInterface
{
    /**
     * @return mixed
     */
    public function getStorageKey();
}
