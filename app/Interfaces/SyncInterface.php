<?php

namespace App\Interfaces;

use App\SyncContext;

/**
 * Interface SyncInterface
 * @package App\Interfaces
 */
interface SyncInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function setSyncContent($data);
}
