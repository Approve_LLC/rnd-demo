<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\EntityFileVersion
 *
 * @property string $id
 * @property string $name
 * @property string|null $entity_file_id
 * @property string $description
 * @property int|null $author_id
 * @property string $file_name
 * @property string|null $object_storage_bucket_id
 * @property string|null $key
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \App\User|null $author
 * @property \App\ObjectStorageBucket $bucket
 * @property \App\EntityFile|null $file
 */
class EntityFileVersion extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];
    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    public $incrementing = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo('App\EntityFile', 'entity_file_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bucket()
    {
        return $this->belongsTo('App\ObjectStorageBucket', 'object_storage_bucket_id');
    }
}
