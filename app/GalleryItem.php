<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\GalleryItem
 *
 * @property string $id
 * @property int $order_idx
 * @property string $gallery_itemable_id
 * @property string $gallery_itemable_type
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property \Illuminate\Database\Eloquent\Model|\Eloquent $owner
 * @property \App\EntityFile $picture
 */
class GalleryItem extends Model
{

    use SoftDeletes;
    
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['order_idx', 'gallery_itemable_id', 'gallery_itemable_type', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function owner()
    {
        return $this->morphTo(null, 'gallery_itemable_type', 'gallery_itemable_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function picture()
    {
        return $this->morphOne('App\EntityFile', 'entity');
    }

    /**
     * @param $model
     * @return GalleryItem
     */
    public static function makeGalleryItem($model)
    {
        $galleryItem = new GalleryItem();
        $galleryItem->owner()->associate($model);
        $galleryItem->order_idx = $model->gallery()->max('order_idx')??1;
        $galleryItem->save();
        return $galleryItem;
    }
}
