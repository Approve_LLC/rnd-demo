<?php

namespace App;

use App\Interfaces\SyncInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\MenuDish
 *
 * @property string $id
 * @property string $dish_id
 * @property string $menu_group_id
 * @property string $menu_type_id
 * @property string $menu_dish_composition_id
 * @property int $ext_id
 * @property int $number_of_servings
 * @property string $name
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property Dish $dish
 * @property MenuGroup $menuGroup
 * @property MenuType $menuType
 * @property MenuDishComposition currentComposition
 * @property MenuDishComposition[] $menuDishCompositions
 */
class MenuDish extends Model implements SyncInterface
{
    use SoftDeletes;

    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['dish_id', 'menu_group_id', 'menu_type_id', 'menu_dish_composition_id', 'ext_id', 'number_of_servings', 'name', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dish()
    {
        return $this->belongsTo('App\Dish');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menuGroup()
    {
        return $this->belongsTo('App\MenuGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menuType()
    {
        return $this->belongsTo('App\MenuType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currentComposition()
    {
        return $this->belongsTo('App\MenuDishComposition', 'menu_dish_composition_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function compositions()
    {
        return $this->hasMany('App\MenuDishComposition');
    }

    /**
     * @return mixed
     */
    public function getOrCreateComposition($packing)
    {
        $composition = !empty($this->currentComposition) ? $this->currentComposition : new MenuDishComposition();
        $composition->menuDish()->associate($this);
        $composition->save();
        $this->currentComposition()->associate($composition);
        $this->save();
        $composition->makeMenuComponents($packing);
        return $composition;
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function setSyncContent($data)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        //это поле отстуствует почти у всех меню_дишей, либо такого диша у нас нет. возможно, что в нормальной базе все ок
        //для проверки предлагаю выделить время для загрузки рабочего дампа
        if (!empty($data->product_description)) {
            $dish = Dish::where(['ext_id' => $data->product_description->id])->first();
            $this->dish()->associate($dish);
        }
        $this->number_of_servings = $data->portions;

        $menuGroup = MenuGroup::where(['ext_id' => $data->menu_group_id])->first();
        $this->menuGroup()->associate($menuGroup);

        $menuType = MenuType::where(['ext_id' => $data->menu_type_id])->first();
        $this->menuType()->associate($menuType);
        $this->save();

        //вот тут далее напишем логику создания новой композиции
        $currentComposition = $this->getOrCreateComposition($data->packing);
        $this->currentComposition()->associate($currentComposition);
    }
}
