<?php

namespace App;

use App\Interfaces\DownloadInterface;
use App\Interfaces\FileManagerInterface;
use App\Interfaces\SyncInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\PromotionSet
 *
 * @property string $id
 * @property integer $ext_id
 * @property string $name
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property \Illuminate\Database\Eloquent\Collection|\App\GalleryItem[] $gallery
 */
class PromotionSet extends Model implements SyncInterface, DownloadInterface, FileManagerInterface
{
    use SoftDeletes;

    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['ext_id', 'name', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function gallery()
    {
        return $this->morphMany('App\GalleryItem', 'gallery_itemable');
    }

    /**
     * @return string
     */
    public function getStorageKey()
    {
        return 'promotion-set';
    }

    /**
     * @param mixed $data
     * @param DownloadContext $context
     */
    public function downloadContent($data, DownloadContext $context)
    {
        //откуда мы берем изображения?
        foreach ($data->gallery as $item) {
            $galleryItem = GalleryItem::makeGalleryItem($this);
            $entityFile = $context
                ->getFileManagerService()
                ->downloadEntityFileVersion($galleryItem, $item, $item->image, $context->bucket);
            $key = $entityFile->current->key;
            $context->getFileManagerService()->uploadFileByUrl($item->image, $key);
            $this->save();
        }
    }

    /**
     * @param $data
     */
    public function setSyncContent($data)
    {
        $this->ext_id = $data->id;
        $this->name = $data->name;
        $this->save();
    }

}
