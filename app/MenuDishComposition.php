<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * App\MenuDishComposition
 *
 * @property string $id
 * @property string $menu_dish_id
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property MenuDish $menuDish
 * @property MenuDish[] $menuDishes
 * @property MenuDishComponent[] $menuDishComponents
 * @property \Illuminate\Database\Eloquent\Collection|\App\MenuDishComponent[] $components
 */
class MenuDishComposition extends Model
{
    use SoftDeletes;

    protected $casts = [
        'id' => 'string'
    ];
    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['menu_dish_id', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menuDish()
    {
        return $this->belongsTo('App\MenuDish');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function components()
    {
        return $this->hasMany('App\MenuDishComponent');
    }

    /**
     * @param $menuPackings
     */
    public function makeMenuComponents($menuPackings)
    {
        if (!empty($menuPackings)) {
            foreach ($menuPackings as $menuPacking) {
                $menuDishComponent = MenuDishComponent::query()->whereHas('packedProduct', function ($query) use ($menuPacking) {
                    $query->where(['ext_id' => $menuPacking->packing_id]);
                })->first();
                $menuDishComponent = !empty($menuDishComponent) ? $menuDishComponent : new MenuDishComponent();
                $menuDishComponent->composition()->associate($this);
                $packedProduct = PackedProduct::query()->where(['ext_id' => $menuPacking->packing_id])->first();
                if (empty($packedProduct)) {
                    throw new NotFoundHttpException("Сперва необходимо загрузить PackedProduct $menuPacking->packing_id!");
                }
                $menuDishComponent->packedProduct()->associate($packedProduct);
                $menuDishComponent->quantity = $menuPacking->quant;
                $menuDishComponent->save();
            }
        }

    }
}
