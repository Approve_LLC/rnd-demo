<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    //'middleware' => 'auth:api'
], function () {
    Route::get('inventories', 'API\InventoryController@index');
    Route::get('inventories/{id}', 'API\InventoryController@show');
    Route::post('inventories', 'API\InventoryController@create');
    Route::put('inventories/{id}', 'API\InventoryController@update');
    Route::delete('inventories/{id}', 'API\InventoryController@delete');
    Route::post('inventories/{id}/upload-image', 'API\InventoryController@uploadImage');
});

Route::group([
    //'middleware' => 'auth:api'
], function () {
    Route::get('chefs', 'API\ChefController@index');
    Route::get('chefs/{id}', 'API\ChefController@show');
    Route::post('chefs', 'API\ChefController@create');
    Route::put('chefs/{id}', 'API\ChefController@update');
    Route::delete('chefs/{id}', 'API\ChefController@delete');
    Route::post('chefs/{id}/upload-image', 'API\ChefController@uploadImage');
});

Route::group([
    //'middleware' => 'auth:api'
], function () {
    Route::get('file-catalog-item', 'API\FileCatalogItemController@index');
    Route::get('file-catalog-item/{id}', 'API\FileCatalogItemController@show');
    Route::post('file-catalog-item', 'API\FileCatalogItemController@create');
    Route::put('file-catalog-item/{id}', 'API\FileCatalogItemController@update');
    Route::delete('file-catalog-item/{id}', 'API\FileCatalogItemController@delete');
    Route::delete('file-catalog-item/{id}', 'API\FileCatalogItemController@delete');
    Route::post('file-catalog-item/{id}/upload-image', 'API\FileCatalogItemController@uploadImage');
});

Route::group([
    //'middleware' => 'auth:api'
], function () {
    Route::get('dishes', 'API\DishController@index');
    Route::get('dishes/{id}', 'API\DishController@show');
    Route::get('dishes/{id}/pdf/test', 'API\DishController@generatePdfTest');
    Route::put('dishes/{id}', 'API\DishController@update');
    Route::post('dishes/{dishId}/recipe-step/{stepId}/upload-image', 'API\DishController@uploadRecipeStepImage');

});

Route::group([
    //'middleware' => 'auth:api'
], function () {
    Route::post('dish-tags', 'API\DishTagController@create');
    Route::put('dish-tags/{id}', 'API\DishTagController@update');
});

Route::group([
    //'middleware' => 'auth:api'
], function () {
    Route::get('promotion-sets', 'API\PromotionSetController@index');
    Route::get('promotion-sets/{id}', 'API\PromotionSetController@show');
    Route::post('promotion-sets/{promotionSetId}/upload-image/{galleryItemId}', 'API\PromotionSetController@uploadImage');
    //соблюдаю ли я стандарты написания api здесь?
    Route::post('promotion-sets/{id}/image-item-order', 'API\PromotionSetController@changeOrder');
});
Route::group([
    //'middleware' => 'auth:api'
], function () {
    Route::get('nomenclatures', 'API\NomenclatureController@index');
    Route::post('nomenclatures/{id}/upload-image', 'API\NomenclatureController@uploadImage');
});