<?php

use App\Dish;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('dish_id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('dish_inventory', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('dish_id');
            $table->uuid('inventory_id');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE dish_inventory ALTER COLUMN dish_id SET DEFAULT uuid_generate_v4();');
        DB::statement('ALTER TABLE dish_inventory ALTER COLUMN inventory_id SET DEFAULT uuid_generate_v4();');

        Schema::create('dishes', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('ext_id');
            $table->string('name');
            $table->uuid('recipe_id')->nullable();
            $table->string('tip_text')->nullable();
            $table->uuid('chef_id')->nullable();
            $table->integer('shelf_life')->nullable();
            $table->integer('net_weight');
            $table->integer('cooking_time');
            $table->enum('difficulty', [1, 2, 3]);
            //делаю нулабл, тк могут отстуствовать
            $table->decimal('protein')->nullable();
            $table->decimal('fat')->nullable();
            $table->decimal('carb')->nullable();
            $table->decimal('calories')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE dishes ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        //DB::statement('ALTER TABLE dishes ALTER COLUMN recipe_id SET DEFAULT uuid_generate_v4();');
        //DB::statement('ALTER TABLE dishes ALTER COLUMN chef_id SET DEFAULT uuid_generate_v4();');

        Schema::table('recipes', function (Blueprint $table) {
            $table->foreign('dish_id')
                ->references('id')->on('dishes')
                ->onDelete('cascade');
        });
        Schema::table('dishes', function (Blueprint $table) {
            $table->foreign('recipe_id')
                ->references('id')->on('recipes')
                ->onDelete('cascade');
            $table->foreign('chef_id')
                ->references('id')->on('chefs')
                ->onDelete('cascade');
        });
        Schema::table('dish_inventory', function (Blueprint $table) {
            $table->foreign('dish_id')
                ->references('id')->on('dishes')
                ->onDelete('cascade');
            $table->foreign('inventory_id')
                ->references('id')->on('inventories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipes', function (Blueprint $table) {
            $table->dropForeign(['dish_id']);
        });
        Schema::table('dishes', function (Blueprint $table) {
            $table->dropForeign(['recipe_id']);
            $table->dropForeign(['chef_id']);
        });
        Schema::table('dish_inventory', function (Blueprint $table) {
            $table->dropForeign(['dish_id']);
            $table->dropForeign(['inventory_id']);
        });
        Schema::dropIfExists('recipes');
        Schema::dropIfExists('dishes');
        Schema::dropIfExists('dish_inventory');
    }
}
