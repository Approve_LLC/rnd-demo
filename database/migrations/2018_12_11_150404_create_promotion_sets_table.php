<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_sets', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('ext_id');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE promotion_sets ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('gallery_items', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('order_idx');
            $table->uuid('gallery_itemable_id');
            $table->string('gallery_itemable_type');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE gallery_items ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_sets');
        Schema::dropIfExists('gallery_items');
    }
}
