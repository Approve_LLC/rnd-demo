<?php

use App\DishPicture;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDishPictureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dish_pictures', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('dish_id');
            $table->enum('angle', [
                0,
                1
            ]);
            $table->enum('destination', [
                0,
                1,
                2
            ]);
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE dish_pictures ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        DB::statement('ALTER TABLE dish_pictures ALTER COLUMN dish_id SET DEFAULT uuid_generate_v4();');

        Schema::table('dish_pictures', function (Blueprint $table) {
            $table->foreign('dish_id')
                ->references('id')->on('dishes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dish_pictures', function (Blueprint $table) {
            $table->dropForeign(['dish_id']);
        });
        Schema::dropIfExists('dish_pictures');
    }
}
