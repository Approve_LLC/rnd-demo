<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_dish_compositions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('menu_dish_id');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE menu_dish_compositions ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('menu_dishes', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('ext_id');
            $table->uuid('dish_id')->nullable();
            $table->integer('number_of_servings')->nullable();
            $table->string('name');
            $table->uuid('menu_group_id')->nullable();
            $table->uuid('menu_type_id')->nullable();
            $table->uuid('menu_dish_composition_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE menu_dishes ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        //DB::statement('ALTER TABLE menu_dishes ALTER COLUMN dish_id SET DEFAULT uuid_generate_v4();');

        Schema::create('menu_dish_components', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('menu_dish_composition_id');
            $table->uuid('packed_product_id');
            $table->decimal('quantity');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE menu_dish_components ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('measure_units', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('ext_id');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE measure_units ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('packed_products', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('ext_id');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE packed_products ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('packed_product_items', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('ext_id');
            $table->uuid('packed_product_id');
            $table->uuid('nomenclature_id');
            $table->decimal('quantity');
            $table->uuid('measure_unit_id');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE packed_product_items ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::table('menu_dishes', function (Blueprint $table) {
            $table->foreign('dish_id')
                ->references('id')->on('dishes')
                ->onDelete('cascade');
            $table->foreign('menu_group_id')
                ->references('id')->on('menu_groups')
                ->onDelete('cascade');
            $table->foreign('menu_type_id')
                ->references('id')->on('menu_types')
                ->onDelete('cascade');
            $table->foreign('menu_dish_composition_id')
                ->references('id')->on('menu_dish_compositions')
                ->onDelete('cascade');
        });

        Schema::table('menu_dish_compositions', function (Blueprint $table) {
            $table->foreign('menu_dish_id')
                ->references('id')->on('menu_dishes')
                ->onDelete('cascade');
        });

        Schema::table('menu_dish_components', function (Blueprint $table) {
            $table->foreign('menu_dish_composition_id')
                ->references('id')->on('menu_dish_compositions')
                ->onDelete('cascade');
            $table->foreign('packed_product_id')
                ->references('id')->on('packed_products')
                ->onDelete('cascade');
        });

        Schema::table('packed_product_items', function (Blueprint $table) {
            $table->foreign('packed_product_id')
                ->references('id')->on('packed_products')
                ->onDelete('cascade');
            $table->foreign('nomenclature_id')
                ->references('id')->on('nomenclatures')
                ->onDelete('cascade');
            $table->foreign('measure_unit_id')
                ->references('id')->on('measure_units')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_dishes', function (Blueprint $table) {
            $table->dropForeign(['dish_id']);
            $table->dropForeign(['menu_group_id']);
            $table->dropForeign(['menu_type_id']);
            $table->dropForeign(['menu_dish_composition_id']);
        });
        Schema::table('menu_dish_compositions', function (Blueprint $table) {
            $table->dropForeign(['menu_dish_id']);
        });
        Schema::table('menu_dish_components', function (Blueprint $table) {
            $table->dropForeign(['menu_dish_composition_id']);
            $table->dropForeign(['packed_product_id']);
        });
        Schema::table('packed_product_items', function (Blueprint $table) {
            $table->dropForeign(['packed_product_id']);
            $table->dropForeign(['nomenclature_id']);
            $table->dropForeign(['measure_unit_id']);
        });
        Schema::dropIfExists('menu_dish_compositions');
        Schema::dropIfExists('menu_dishes');
        Schema::dropIfExists('menu_dish_components');
        Schema::dropIfExists('measure_units');
        Schema::dropIfExists('packed_products');
        Schema::dropIfExists('packed_product_items');
    }
}
