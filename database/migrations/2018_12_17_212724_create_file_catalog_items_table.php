<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileCatalogItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_catalog_items', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->boolean('is_folder');
            $table->uuid('parent_id');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE file_catalog_items ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        DB::statement('ALTER TABLE file_catalog_items ALTER COLUMN parent_id SET DEFAULT uuid_generate_v4();');
        Schema::table('file_catalog_items', function (Blueprint $table) {
            $table->foreign('parent_id')
                ->references('id')->on('file_catalog_items')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('file_catalog_items', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
        });
        Schema::dropIfExists('file_catalog_items');
    }
}
