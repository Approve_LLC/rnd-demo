<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_groups', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('ext_id');
            $table->string('name');
            $table->uuid('parent_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE menu_groups ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        Schema::table('menu_groups', function (Blueprint $table) {
            $table->foreign('parent_id')
                ->references('id')->on('menu_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_groups', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
        });
        Schema::dropIfExists('menu_groups');
    }
}
