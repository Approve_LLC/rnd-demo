<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntitiesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_files', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('entity_id');
            $table->string('entity_type');
            $table->uuid('entity_file_version_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['entity_type', 'entity_id']);
        });
        DB::statement('ALTER TABLE entity_files ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        DB::statement('ALTER TABLE entity_files ALTER COLUMN entity_id SET DEFAULT uuid_generate_v4();');

        Schema::create('entity_file_versions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->uuid('entity_file_id');
            //при импорте порой бывает
            //Not null violation: 7 ERROR:  null value in column "description" violates not-null constraint
            $table->text('description')->nullable();//тут может быть много символов
            $table->integer('author_id')->nullable();
            $table->string('file_name');
            $table->uuid('object_storage_bucket_id');
            $table->string('key');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE entity_file_versions ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        //DB::statement('ALTER TABLE entity_file_versions ALTER COLUMN entity_file_id SET DEFAULT uuid_generate_v4();');
        DB::statement('ALTER TABLE entity_file_versions ALTER COLUMN object_storage_bucket_id SET DEFAULT uuid_generate_v4();');

        Schema::create('object_storage_buckets', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('host');
            $table->string('bucket');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE object_storage_buckets ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::table('entity_file_versions', function (Blueprint $table) {
            $table->foreign('object_storage_bucket_id')
                ->references('id')->on('object_storage_buckets')
                ->onDelete('cascade');
            $table->foreign('author_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('entity_file_id')
                ->references('id')->on('entity_files')
                ->onDelete('cascade');
        });

        Schema::table('entity_files', function (Blueprint $table) {
            $table->foreign('entity_file_version_id')
                ->references('id')->on('entity_file_versions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entity_file_versions', function (Blueprint $table) {
            $table->dropForeign(['object_storage_bucket_id']);
            $table->dropForeign(['author_id']);
            $table->dropForeign(['entity_file_id']);
        });
        Schema::table('entity_files', function (Blueprint $table) {
            $table->dropForeign(['entity_file_version_id']);
        });
        Schema::dropIfExists('entity_files');
        Schema::dropIfExists('entity_file_versions');
        Schema::dropIfExists('object_storage_buckets');

    }
}
