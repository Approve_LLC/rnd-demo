<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNomenclatureTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nomenclature_groups', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('ext_id');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE nomenclature_groups ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('nomenclatures', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('ext_id');
            $table->string('name');
            $table->uuid('group_id')->nullable();
            $table->boolean('is_packing');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE nomenclatures ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        DB::statement('ALTER TABLE nomenclatures ALTER COLUMN group_id SET DEFAULT uuid_generate_v4();');
        Schema::table('nomenclatures', function (Blueprint $table) {
            $table->foreign('group_id')
                ->references('id')->on('nomenclature_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nomenclatures', function (Blueprint $table) {
               $table->dropForeign(['group_id']);
        });
        Schema::dropIfExists('nomenclatures');
        Schema::dropIfExists('nomenclature_groups');
    }
}
