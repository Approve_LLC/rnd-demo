<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_steps', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('recipe_id');
            $table->integer('step_number');
            $table->text('description');
            $table->integer('duration');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE recipe_steps ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
        DB::statement('ALTER TABLE recipe_steps ALTER COLUMN recipe_id SET DEFAULT uuid_generate_v4();');

        Schema::table('recipe_steps', function (Blueprint $table) {
            $table->foreign('recipe_id')
                ->references('id')->on('recipes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipe_steps', function (Blueprint $table) {
            $table->dropForeign(['recipe_id']);
        });
        Schema::dropIfExists('recipe_steps');
    }
}
