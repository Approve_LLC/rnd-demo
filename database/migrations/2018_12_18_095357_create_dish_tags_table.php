<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDishTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dish_tags', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('ext_id');
            $table->string('name');
            $table->boolean('is_shown');
            $table->boolean('is_used_for_filtering');
            $table->boolean('is_used_for_sorting');
            $table->softDeletes();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE dish_tags ALTER COLUMN id SET DEFAULT uuid_generate_v4();');

        Schema::create('dish_dish_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('dish_id');
            $table->uuid('dish_tag_id');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE dish_dish_tag ALTER COLUMN dish_id SET DEFAULT uuid_generate_v4();');
        DB::statement('ALTER TABLE dish_dish_tag ALTER COLUMN dish_tag_id SET DEFAULT uuid_generate_v4();');

        Schema::table('dish_dish_tag', function (Blueprint $table) {
            $table->foreign('dish_id')
                ->references('id')->on('dishes')
                ->onDelete('cascade');
            $table->foreign('dish_tag_id')
                ->references('id')->on('dish_tags')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dish_dish_tag', function (Blueprint $table) {
            $table->dropForeign(['dish_id']);
            $table->dropForeign(['dish_tag_id']);
        });
        Schema::dropIfExists('dish_dish_tag');
        Schema::dropIfExists('dish_tags');
    }
}
