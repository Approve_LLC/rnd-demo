<?php

use Illuminate\Database\Seeder;

class FirstObjectStorageBucket extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('object_storage_buckets')->insert([
            'host' => config('app.minio_endpoint'),
            'bucket' => config('app.bucket_rnd_name')
        ]);
    }
}